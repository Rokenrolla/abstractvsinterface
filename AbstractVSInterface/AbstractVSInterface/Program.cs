﻿using System;

namespace AbstractVSInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            string shape = "none";
            while (shape != "rectangle" && shape != "circle" && shape != "square" && shape != "cube" && shape != "box" && shape != "sphere")
            {
                Console.WriteLine("Please select your shape: sphere, circle, cube, square, box or rectangle?");
                shape = Convert.ToString(Console.ReadLine());
            }

            if (shape == "rectangle")
            {
                Console.WriteLine("Height =");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Width =");
                int y = Convert.ToInt32(Console.ReadLine());
                Rectangle r1 = new Rectangle(x, y);
                Console.WriteLine("Perimeter = " + r1.PerimeterCalculator());
                Console.WriteLine("Area = " + r1.AreaCalculator());
            }
            if (shape == "circle")
            {
                Console.WriteLine("Radius =");
                int r = Convert.ToInt32(Console.ReadLine());
                Circle c1 = new Circle(r);
                Console.WriteLine("Perimeter = " + c1.PerimeterCalculator());
                Console.WriteLine("Area = " + c1.AreaCalculator());
            }
            if (shape == "square")
            {
                Console.WriteLine("Side =");
                int s = Convert.ToInt32(Console.ReadLine());
                Square s1 = new Square(s);
                Console.WriteLine("Perimeter = " + s1.PerimeterCalculator());
                Console.WriteLine("Area = " + s1.AreaCalculator());

            }
            if (shape == "cube")
            {
                Console.WriteLine("Side =");
                int s = Convert.ToInt32(Console.ReadLine());
                Cube cube = new Cube(s);
                Console.WriteLine("Cube's perimeter =" + cube.PerimeterCaculator());
                Console.WriteLine("Cube's volume =" + cube.VolumeCalculator());

            }
            if (shape == "box")
            {
                Console.WriteLine("Heigth =");
                int x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Width =");
                int y = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Length =");
                int z = Convert.ToInt32(Console.ReadLine());
                Box box = new Box(x, y, z);
                Console.WriteLine("Cube's perimeter =" + box.PerimeterCaculator());
                Console.WriteLine("Cube's volume =" + box.VolumeCalculator());
            }
            if (shape == "sphere")
            {
                Console.WriteLine("Radius =");
                int r = Convert.ToInt32(Console.ReadLine());
                Sphere sphere = new Sphere(r);
                Console.WriteLine("Sphere's perimeter = the Sphere doesn't have one");
                Console.WriteLine("Sphere's volume =" + sphere.VolumeCalculator());
            }
        }

        public abstract class Shape
        {
            public int Length { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public int Radius { get; set; }
            public int Side { get; set; }
        }

        interface Iprop
        {
            public abstract double AreaCalculator();
            public abstract double PerimeterCalculator();
        }

        interface I3D
        {
            public abstract double VolumeCalculator();
            public abstract double PerimeterCaculator();
        }

        public class Rectangle : Shape, Iprop
        {
            public Rectangle()
            {
            }

            public Rectangle(int height, int width)
            {
                Height = height;
                Width = width;
            }
            public double AreaCalculator()
            {
                return Height * Width;
            }
            public double PerimeterCalculator()
            {
                return 2 * (Height + Width);
            }
        }

        public class Circle : Shape, Iprop
        {
            public Circle()
            {

            }
            public Circle(int radius)
            {
                Radius = radius;
            }
            public double AreaCalculator()
            {
                return Math.PI * Radius * Radius;
            }
            public double PerimeterCalculator()
            {
                return 2 * Radius * Math.PI;
            }
        }

        public class Square : Shape, Iprop
        {
            public Square()
            {
            }

            public Square(int side)
            {
                Side = side;
            }
            public double AreaCalculator()
            {
                return Side * Side;
            }
            public double PerimeterCalculator()
            {
                return 4 * Side;
            }
        }


        public class Cube : Square, I3D
        {
            public Cube(int side)
            {
                Side = side;
            }

            public double PerimeterCaculator()
            {
                return 12 * Side;
            }

            public double VolumeCalculator()
            {
                return this.AreaCalculator() * Side;
            }
        }

        public class Box : Rectangle, I3D
        {
            public Box(int heigth, int width, int length)
            {
                Height = heigth;
                Width = width;
                Length = length;
            }

            public double PerimeterCaculator()
            {
                return 4 * (Height + Width + Length);
            }

            public double VolumeCalculator()
            {
                return this.AreaCalculator() * Length;
            }
        }
        public class Sphere : Circle, I3D
        {
            public Sphere(int radius)
            {
                Radius = radius;
            }

            public double PerimeterCaculator()
            {
                return 0;
            }

            public double VolumeCalculator()
            {
                return this.AreaCalculator() * 4 * Radius / 3;
            }
        }
    }
}
